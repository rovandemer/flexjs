/*

Flex JS, third version

Author : UnSavantFou

*/

// Importing modules
const path = require("path");
const fs = require("fs");

// Importing classes
const Flex = require("./classes/Flex");
const FlexCommand = require("./classes/FlexCommand");
const FlexEvent = require("./classes/FlexEvent");

// Importing config
const config = require("./config.json");

// Creating the Flex client
const flex = new Flex(config.python);

// console.log(flex.checkImbricatedTokens());
// console.log(flex.checkTokens("for i in range(10): print(i)"));

console.log(flex.tokenizeFile(path.join(__dirname, "/sample/main.py")));

// Loading commands
// TODO

// Loading events
// TODO
