// Description: Flex class
//


// Modules
const fs = require("fs");

/**
 * @param {String} string
 * @returns {Array} occurences
 * @description Returns an array of the occurences of a substring in a string
 * @example "Hello World".everyOccurence("l") returns [2, 3, 9]
 **/
String.prototype.everyOccurence = function (subString) {

    // Creating the occurences array
    let occurences = [];

    // Looping through the array
    for (let i = 0; i < this.length; i++) {

        // Checking if the substring is present
        if (this.substring(i, i + subString.length) === subString) {

            // Adding the occurence to the occurences array
            occurences.push(i);

        }

    }

    // Returning the occurences array 
    return occurences;

}

    

class Flex {

    // Constructor
    /**
     * @param {Object} config
     * @param {Array} config.tokens
    **/
    constructor(config) {
        this.config = config;
    }



    // Check if there are imbricated tokens
    /**
     * @returns {Array} imbricatedTokens
    **/ 
    checkImbricatedTokens() { // If we know in advance if there are imbricated tokens, we can avoid useless loops

        // Creating the imbricated tokens array
        let imbricatedTokens = [];

        // Looping through the tokens
        this.config.tokens.forEach(token => {

            // Looping through the tokens
            this.config.tokens.forEach(token2 => {

                // Checking if the token are imbricated
                if (token.val.includes(token2.val) && token !== token2) {

                    // Adding the token to the imbricated tokens array
                    // We have to get the offset of the token
                    // Exemple :
                    // "print" has "in" in it
                    // The offset of "in" is 2 (3-1)
                    
                    // But there could be more than one occurence of the sub-token
                    var offset = token.val.everyOccurence(token2.val);
                    
                    imbricatedTokens.push({ token: token, subtoken: token2, offset: offset });

                }

            });

        });

        // Returning the imbricated tokens array

        return imbricatedTokens;

    }

    
    // Check for tokens in a string
    /**
     * @param {String} string
     * @returns {Array} tokens
    **/
    checkTokens(string) {

        // Creating the tokens array
        let tokens = {};

        // Looping through the tokens
        this.config.tokens.forEach(token => {

            // Checking if the token is present in the string
            tokens[token.type] = string.everyOccurence(token.val);

        });

        // There could be a problem if there are imbricated tokens
        // We check if both tokens are present in the string
        // If they are, we check if the offset

        // Principle of the Offset :
        // print
        //   in
        // 01345
        // If print is detected in position 4, and in is detected in position 4+2=6, we remove the token in from the tokens array
        const imbricatedTokens = this.checkImbricatedTokens();

        // We make a copy, because there could be multiple imbricated tokens :
        // print
        //   in
        //   int
        // 012345
        // If we delete for in, there could be a conflict with int depending on when the loop is executed
        let tokensCopy = JSON.parse(JSON.stringify(tokens));

        // Looping through the imbricated tokens
        imbricatedTokens.forEach(imbricatedToken => {

            for (var i=0;i<tokens[imbricatedToken.token.type].length;i++) {

                // For each offset, we check if the subtoken is present
                for (var j=0;j<imbricatedToken.offset.length;j++) {

                    // If the subtoken is present, we remove it from the tokens array
                    let wantedToken = tokens[imbricatedToken.token.type][i] + imbricatedToken.offset[j];

                    if (tokens[imbricatedToken.subtoken.type].includes(wantedToken) && tokensCopy[imbricatedToken.subtoken.type].includes(wantedToken)) {

                        // Removing the token from the tokens array
                        tokensCopy[imbricatedToken.subtoken.type].splice(tokensCopy[imbricatedToken.subtoken.type].indexOf(wantedToken), 1);

                    }

                }

            }

        });

        // We replace the tokens array with the tokensCopy array
        tokens = tokensCopy;

        // Returning the tokens array
        return tokens;

    }


    tokenizeFile(file) {

        // Reading the file
        let content = fs.readFileSync(file, "utf8");

        // Tokenizing the file
        // For each line, we check for tokens
        let lines = content.split("\n");
        let tokens = [];
        for (var i=0;i<lines.length;i++) {

            // Checking for tokens
            tokens.push(this.checkTokens(lines[i]));

        }

        // Tokens array is like following :
        // [
        //     { -> line 1
        //         "comments": [ -> token 1 ]
        //         "print": [ -> token 2 ]
        //     },...
        // ]

        // There could be empty sub-arrays
        // We remove them
        for (var i=0;i<tokens.length;i++) {

            // Looping through the tokens
            for (var token in tokens[i]) {

                // Checking if the token is empty
                if (tokens[i][token].length === 0) {

                    // Removing the token
                    delete tokens[i][token];

                }

            }

        }


        // Returning the tokens array
        return tokens;

    }

}

// Export
module.exports = Flex;